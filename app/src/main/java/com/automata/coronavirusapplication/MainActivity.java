package com.automata.coronavirusapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    String url="https://www.worldometers.info/coronavirus/";

    TextView totalcases,totaldeaths,totalrecover,activecases,closedcases,mild,deaths,discharge,critical;
    String tcases,tdeaths,trecover,tactive,tclosed,tmild,tdeaths2,tdischarge,tcritical;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        __init__();
        new doinbackground().execute();

    }
    public class doinbackground extends  AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressBar.setVisibility(View.GONE);
            setvalues();
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document document=Jsoup.connect(url)
                        .timeout(30*1000)
                        .get();

                Elements element=document.select("div.maincounter-number");
                int i=0;
                for(Element e: element){
                    if(i==0){
                        tcases=e.text();
                        Log.d("abc", "doInBackground: "+tcases);
                    }
                    else if(i==1){
                        tdeaths=e.text();
                        Log.d("abc", "doInBackground: "+tdeaths);
                    }
                    else if(i==2){
                        trecover=e.text();
                        Log.d("abc", "doInBackground: "+trecover);
                    }
                    i+=1;
                }

                Elements element1=document.select("div.number-table-main");
                i=0;
                for(Element e: element1){
                    if(i==0){
                        tactive=e.text();
                        Log.d("abc", "doInBackground: "+e.text());
                    }else if(i==1){
                        tclosed=e.text();
                        Log.d("abc", "doInBackground: "+e.text());
                    }
                    i+=1;
                }
                i=0;
                Elements element2=document.select("span.number-table");
                for(Element e : element2){
                    Log.d("abc", "doInBackground: "+e.text());
                    if(i==0){
                        tmild=e.text();
                    }else if(i==1){
                        tcritical=e.text();
                    }if(i==2){
                        tdischarge=e.text();
                    }else if(i==3){
                        tdeaths2=e.text();
                    }
                    i+=1;
                }



            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }
    }
    public void setvalues(){
        totalcases.setText(tcases);
        totaldeaths.setText(tdeaths);
        totalrecover.setText(trecover);
        activecases.setText(tactive);
        closedcases.setText(tdeaths);
        mild.setText(tmild);
        critical.setText(tcritical);
        discharge.setText(tdischarge);
        deaths.setText(tdeaths2);


    }
    public void __init__(){
        totalcases=findViewById(R.id.totalcases);
        totaldeaths=findViewById(R.id.totaldeaths);
        totalrecover=findViewById(R.id.totalrecover);
        progressBar=findViewById(R.id.progressbar);
        activecases=findViewById(R.id.activecases);
        closedcases=findViewById(R.id.closedcases);
        mild=findViewById(R.id.mild);
        critical=findViewById(R.id.critical);
        discharge=findViewById(R.id.discharge);
        deaths=findViewById(R.id.deaths);


    }





}
